//
//  ArgumentManager.swift
//  ArgumentManagerExample
//
//  Created by Zakk Hoyt on 4/18/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import Foundation

public let ArgumentManagerArgumentKey = "argument"

public extension Notification.Name {
    static public let ArgumentManagerArgumentsDidChange = Notification.Name("ArgumentManagerArgumentsDidChange")
}

public class ArgumentManager {
    
    public static let shared = ArgumentManager()
    
    fileprivate var _arguments: [Argument] = []
    public var arguments: [Argument] {
        get {
            return _arguments
        }
        set {
            _arguments = newValue
            setupPresence()
        }
    }

    /// Enable/Disable an argument in the .userDefault category
    public func modifyUserDefaultsArgument(_ argument: Argument, enable: Bool) {
        if enable {
            ArgumentManagerUserDefaults.insertArgument(argument)
        } else {
            ArgumentManagerUserDefaults.removeArgument(argument)
        }
        setupPresenceFor(argument: argument)
        notifyWithArgument(argument)
    }
    
    /// Enable/Disable an argument in the .runtime category
    public func modifyRuntimeArgument(_ argument: Argument, enable: Bool) {
        if enable {
            argument.presence.insert(.runtime)
        } else {
            argument.presence.remove(.runtime)
        }
        notifyWithArgument(argument)
    }
    
//    public func enableArgument(_ argumentString: String, presence: Presence) {
//        for argument in arguments {
//            if argument.title == argumentString {
//                argument.presence = argument.presence.union(presence)
//            }
//        }
//    }
//    
//    public func disableArgument(_ argumentString: String, presence: Presence) {
//        for argument in arguments {
//            if argument.title == argumentString {
//                argument.presence.remove(presence)
//            }
//        }
//    }

    
    /// Check if an argument is enabled or not
    public func argumentEnabled(_ argumentString: String) -> Bool {
        for argument in arguments {
            if argument.title == argumentString {
                return !argument.presence.isEmpty
            }
        }
        return false
    }
    
    /// Check if a list or arguments are enabled or not
    public func argumentsEnabled(_ argumentStrings: [String]) -> Bool {
        for agrumentString in argumentStrings {
            if !argumentEnabled(agrumentString) {
                return false
            }
        }
        return true
    }

    
    private func setupPresence() {
        for argument in arguments {
            setupPresenceFor(argument: argument)
        }
    }
    
    private func setupPresenceFor(argument: Argument) {
        // Xcode
        if ProcessInfo.processInfo.arguments.contains(argument.title) {
            argument.presence.insert(.xcode)
        } else {
            argument.presence.remove(.xcode)
        }
        
        // UserDefaults
        if ArgumentManagerUserDefaults.isModeEnabled(argument.title) {
            argument.presence.insert(.userDefaults)
        } else {
            argument.presence.remove(.userDefaults)
        }
    }
    
    private func notifyWithArgument(_ argument: Argument) {
        let userInfo: [String: Any] = [ArgumentManagerArgumentKey: argument]
        NotificationCenter.default.post(name: .ArgumentManagerArgumentsDidChange, object: self, userInfo: userInfo)
    }
}


class ArgumentManagerUserDefaults {
    static let ArgumentManagerUserDefaultsKey = "ArgumentManagerUserDefaults"
    
    class func printArguments() {
        if let modes = Foundation.UserDefaults.standard.object(forKey: ArgumentManagerUserDefaultsKey) as? String {
            print("~~ UserDefault arguments: " + modes)
        } else {
            print("~~ UserDefault arguments: -")
        }
    }
    
    class func isModeEnabled(_ mode: String) -> Bool {
        if let modes = Foundation.UserDefaults.standard.object(forKey: ArgumentManagerUserDefaultsKey) as? String {
            if modes.uppercased().contains(mode.uppercased()) {
                return true
            }
        }
        return false
    }
    
    class func modesString() -> String? {
        return Foundation.UserDefaults.standard.object(forKey: ArgumentManagerUserDefaultsKey) as? String
    }
    
    class func insertArgument(_ argument: Argument) {
        if var modes = Foundation.UserDefaults.standard.object(forKey: ArgumentManagerUserDefaultsKey) as? String {
            if modes.contains(argument.title) {
                // Already contains argument
                print("UserDefaults: Already contains argument: " + argument.title)
                return
            } else {
                // Append and write back
                
                modes += " " + argument.title
                Foundation.UserDefaults.standard.set(modes, forKey: ArgumentManagerUserDefaultsKey)
                Foundation.UserDefaults.standard.synchronize()
                print("UserDefaults: Appending argument: " + argument.title)
            }
        } else {
            // Add new default
            Foundation.UserDefaults.standard.set(argument.title, forKey: ArgumentManagerUserDefaultsKey)
            Foundation.UserDefaults.standard.synchronize()
            print("UserDefaults: Created new argument list with: " + argument.title)
        }
    }
    
    class func removeArgument(_ argument: Argument) {
        if var modes = Foundation.UserDefaults.standard.object(forKey: ArgumentManagerUserDefaultsKey) as? String {
            if modes.contains(argument.title) {
                modes = modes.replacingOccurrences(of: argument.title, with: "")
                Foundation.UserDefaults.standard.set(modes, forKey: ArgumentManagerUserDefaultsKey)
                Foundation.UserDefaults.standard.synchronize()
                print("UserDefaults: Removed argument: " + argument.title)
            } else {
                print("UserDefaults: Argument could not be removed (not found): " + argument.title)
            }
        } else {
            print("UserDefaults: Argument could not be removed (nothing stored in user defaults): " + argument.title)
        }
    }
}



