//
//  ArgumentManagerTableViewController.swift
//  ArgumentManagerExample
//
//  Created by Zakk Hoyt on 4/18/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import UIKit

public class ArgumentManagerTableViewController: UITableViewController {

    public var argumentManager: ArgumentManager = ArgumentManager.shared
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Arguments"
        setupNavBar()
        setupTableView()
    }

    private func setupNavBar() {
        if let nc = navigationController {
            return
        }

        if let tc = tabBarController {
            return
        }


        // We have no container so add a close button at the top
        let bundle = Bundle(for: ArgumentManagerTableHeaderView.self)
        guard let tableHeaderView = bundle.loadNibNamed(ArgumentManagerTableHeaderViewIdentifier, owner: self, options: [:])?.first as? ArgumentManagerTableHeaderView else {
            assert(false, "")
            return
        }
        tableHeaderView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 44)
        tableHeaderView.closeButtonTapped = { _ in
            self.dismiss(animated: true, completion: nil)
        }
        tableView.tableHeaderView = tableHeaderView

    }

    private func setupTableView() {
        func setupCells() {
            let bundle = Bundle(for: ArgumentTableViewCell.self)
            let nib = UINib(nibName: ArgumentTableViewCellIdentifier, bundle: bundle)
            tableView.register(nib, forCellReuseIdentifier: ArgumentTableViewCellIdentifier)
        }
        setupCells()
        
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100.0
    }


    // MARK: Impelements UITableViewDataSource

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return argumentManager.arguments.count
    }


    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ArgumentTableViewCellIdentifier, for: indexPath) as? ArgumentTableViewCell else {
            assert(false, "Failed to dequeue ArgumentTableViewCell")
            return UITableViewCell()
        }
        let argument = argumentManager.arguments[indexPath.row]
        cell.argument = argument
        cell.xcodeButtonTapped = {/* [weak self]*/ _ in
            // Xcode args cannot be changed at runtime
        }
        cell.userDefaultsButtonTapped = { [weak self] in
            if argument.presence.contains(.userDefaults) {
                self?.argumentManager.modifyUserDefaultsArgument(argument, enable: false)
            } else {
                self?.argumentManager.modifyUserDefaultsArgument(argument, enable: true)
            }
            
            self?.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        cell.runtimeButtonTapped = { [weak self] in
            if argument.presence.contains(.runtime) {
                self?.argumentManager.modifyRuntimeArgument(argument, enable: false)
            } else {
                self?.argumentManager.modifyRuntimeArgument(argument, enable: true)
            }
            self?.tableView.reloadRows(at: [indexPath], with: .automatic)
        }

        return cell
    }



}
