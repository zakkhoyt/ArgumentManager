//
//  Argument.swift
//  ArgumentManagerExample
//
//  Created by Zakk Hoyt on 4/18/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import Foundation


public struct Presence: OptionSet {
    public let rawValue: UInt
    static public let xcode = Presence(rawValue: 1 << 0)
    static public let userDefaults = Presence(rawValue: 1 << 1)
    static public let runtime  = Presence(rawValue: 1 << 2)
    
    public init(rawValue: UInt) {
        self.rawValue = rawValue
    }
}


public class Argument {
    var title: String
    var description: String
    var presence: Presence = []
    
    public init(title: String, description: String) {
        self.title = title
        self.description = description
    }
    

}
