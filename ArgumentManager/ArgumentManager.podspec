
Pod::Spec.new do |s|
  s.name         = "ArgumentManager"
  s.version      = "1.0.5"
  s.summary      = "A visual way to manage arguments to your iOS application"
  s.author        = { "Zakk Hoyt" => "vaporwarewolf@gmail.com" }
  s.homepage      = "https://gitlab.com/zakkhoyt/ArgumentManager"
  s.platforms = { :ios => 10.0
                }
  s.license = { :type => 'MIT',
                :text =>  <<-LICENSE
                  Copyright 2017. Zakk hoyt.
                          LICENSE
              }
  s.source       = { :git => 'https://gitlab.com/zakkhoyt/ArgumentManager.git',
                    :tag =>  "#{s.version}" }
  s.source_files  = 'ArgumentManager/**/*.{swift}'
  s.resource_bundles = {
                    'ArgumentManager' => ['ArgumentManager/**/*.{xib,xcassets}']
                    }
  s.requires_arc = true
  s.ios.deployment_target = '10.0'
end
