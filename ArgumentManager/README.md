### ArgumentManager

A visual way to manage arguments to your iOS application. At a glance, see which arguments have been passed to your app by Xcode. Use/persists those same arguments by storing them in UserDefaults, or for the current run-time.

### Description

When developing an app, it is common to add arguments to Xcode which modifies the behavior of your app

![](http://i.imgur.com/G3bzoUv.png)


ArgumentManager makes it easy to extend these arguments to your QA users. The developer can still pass arguments in via Xcode scheme as normal. The included table view allows your users to toggle arguments on and off in both non-volatile and volatile fashions.

![](http://i.imgur.com/dyXFzPu.png)

##### Legend

Xc - Xcode arguments which are passed in via Xcode IDE.

UD - UserDefaults. Arguments are stored in UserDefaults (non-volatile memory).

RT - RunTime. Arguments are stored in volatile memory and will be lost when the app is terminated.

##### Apply Arguments

Check if these argument are enabled to modify your code flow:

````
if ArgumentManager.shared.argumentEnabled(Arguments.darkMode) {
    // Configure your view for DarkMode
} else {
    // Configure your view for LightMode
}
````

### Usage

1.) Clone this repository and then copy /ArgumentManager to your project.

2.) It is recomended to create a class which defines your arguments and provides a setup function to configure ArgumentManager:

````
class Arguments {

    static let alertErrors = "ALERT_ERRORS"
    static let darkMode = "DARK_MODE"
    static let beVerbose = "BE_VERBOSE"

    class func setup() {
        let arguments: [Argument] = [
            Argument(title: alertErrors, description: "Present error in an UIAlertController."),
            Argument(title: darkMode, description: "Render the app using an alternative color set."),
            Argument(title: beVerbose, description: "When printing text or descriptions, be very very very verbose, including this. When printing text or descriptions, be very very very verbose, including this. When printing text or descriptions, be very very very verbose, including this. When printing text or descriptions, be very very very verbose, including this.")
        ]
        ArgumentManager.shared.arguments = arguments
    }
}

````

3.) Call setup on the above class from your AppDelegate or other appropriate location:

````
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    Arguments.setup()
    return true
}
````


* You can expose the included UITableViewController (shown above) using typical navigationController, tabController, or modal techniques.
* Your users can toggle arguments on/off by tapping the UD (UserDefaults) or RT (Runtime) buttons.
* Developers can set Xcode arguments by adding them to the target's scheme (shown above). Tapping on Xc button does nothing.

````
@IBAction func settingsBarbAction(_ sender: Any) {
    let argumentsViewController = ArgumentManagerTableViewController()
    _ = self.navigationController?.pushViewController(argumentsViewController, animated: true)
}
````

* Check for arguments to modify your code flow:

````
if ArgumentManager.shared.argumentEnabled(Arguments.darkMode) {
    // Configure your view for DarkMode
} else {
    // Configure your view for LightMode
}
````

* Changes may be observed via Notifications

````
NotificationCenter.default.addObserver(self, selector: #selector(argumentsDidChange), name: .ArgumentManagerArgumentsDidChange, object: nil)
````

### Example

Clone this repository and open ArgumentManagerExample.xcodeproj
