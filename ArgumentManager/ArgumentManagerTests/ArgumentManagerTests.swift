//
//  ArgumentManagerTests.swift
//  ArgumentManagerTests
//
//  Created by Zakk Hoyt on 4/19/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import XCTest

@testable import ArgumentManager

class ArgumentManagerTests: XCTestCase {
    
    class Arguments {
        
        static let alertErrors = "XCT_ALERT_ERRORS"
        static let darkMode = "XCT_DARK_MODE"
        static let beVerbose = "XCT_BE_VERBOSE"
        
        class func setup(argumentManager: ArgumentManager) {
            let arguments: [Argument] = [
                Argument(title: alertErrors, description: "Present error in an UIAlertController."),
                Argument(title: darkMode, description: "Render the app using an alternative color set."),
                Argument(title: beVerbose, description: "When printing text or descriptions, be very very very verbose, including this. When printing text or descriptions, be very very very verbose, including this. When printing text or descriptions, be very very very verbose, including this. When printing text or descriptions, be very very very verbose, including this.")
            ]
            argumentManager.arguments = arguments
        }
    }
    
    
    var argumentManager: ArgumentManager!
    override func setUp() {
        super.setUp()
        
        argumentManager = ArgumentManager()
        Arguments.setup(argumentManager: argumentManager)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExampleConfig() {
        
        
        ArgumentManagerUserDefaults.printArguments()
        
        // Disable all args and test API
        disableAll()
        XCTAssertFalse(argumentManager.argumentEnabled(Arguments.beVerbose))
        XCTAssertFalse(argumentManager.argumentEnabled(Arguments.darkMode))
        XCTAssertFalse(argumentManager.argumentEnabled(Arguments.alertErrors))
        
        // Enable all args in the userDefault category and test API
        enableUserDefaults()
        XCTAssertTrue(argumentManager.argumentEnabled(Arguments.beVerbose))
        XCTAssertTrue(argumentManager.argumentEnabled(Arguments.darkMode))
        XCTAssertTrue(argumentManager.argumentEnabled(Arguments.alertErrors))
        
        // Disable all args and test API
        disableAll()
        XCTAssertFalse(argumentManager.argumentEnabled(Arguments.beVerbose))
        XCTAssertFalse(argumentManager.argumentEnabled(Arguments.darkMode))
        XCTAssertFalse(argumentManager.argumentEnabled(Arguments.alertErrors))
        
        // Enable all args in the runtime category and test API
        enableRunTime()
        XCTAssertTrue(argumentManager.argumentEnabled(Arguments.beVerbose))
        XCTAssertTrue(argumentManager.argumentEnabled(Arguments.darkMode))
        XCTAssertTrue(argumentManager.argumentEnabled(Arguments.alertErrors))
        
        // Disable all args and test API
        disableAll()
        XCTAssertFalse(argumentManager.argumentEnabled(Arguments.beVerbose))
        XCTAssertFalse(argumentManager.argumentEnabled(Arguments.darkMode))
        XCTAssertFalse(argumentManager.argumentEnabled(Arguments.alertErrors))
        
        
        // Enable all args in the userDefault category and test API
        enableUserDefaults()
        XCTAssertTrue(argumentManager.argumentsEnabled([Arguments.beVerbose, Arguments.darkMode, Arguments.alertErrors]))
        
        // Disable all args and test API
        disableAll()
        XCTAssertFalse(argumentManager.argumentEnabled(Arguments.beVerbose))
        XCTAssertFalse(argumentManager.argumentEnabled(Arguments.darkMode))
        XCTAssertFalse(argumentManager.argumentEnabled(Arguments.alertErrors))
        
        // Enable all args in the runtime category and test API
        enableRunTime()
        XCTAssertTrue(argumentManager.argumentsEnabled([Arguments.beVerbose, Arguments.darkMode, Arguments.alertErrors]))
        
        
    }
    
    
    private func disableAll() {
        for argument in argumentManager.arguments {
            argumentManager.modifyRuntimeArgument(argument, enable: false)
            argumentManager.modifyUserDefaultsArgument(argument, enable: false)
        }
    }
    
    private func enableUserDefaults() {
        for argument in argumentManager.arguments {
            argumentManager.modifyUserDefaultsArgument(argument, enable: true)
        }
    }
    
    private func enableRunTime() {
        for argument in argumentManager.arguments {
            argumentManager.modifyRuntimeArgument(argument, enable: true)
        }
    }
    
    
    
}
